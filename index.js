import {
  getAllProducts,
  getProductById,
  generateNewProduct,
  updateProductById,
  deleteProductById,
} from "./productService.js";

import express from "express";
const app = express();
const port = 8080;
const baseUrl = "/api";
const baseProduct = "/productos";

const routes = express.Router();

routes.get(baseProduct + "/listar", async (req, res) => {
  /**
   * Devuelve array de productos
   */
  const products = getAllProducts();
  if (products.length > 0) {
    res.send({ products });
  } else {
    res.status(400).send({ error: "No hay productos cargados" });
  }
});

routes.get(baseProduct + "/listar/:id([0-9]+)", async (req, res) => {
  /**
   * Devuelve producto por id
   */
  const productId = Number(req.params.id);
  const product = getProductById(productId);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
});

routes.post(baseProduct + "/guardar/", (req, res) => {
  /**
   * Devuelve el producto incorporado
   */

  const { title, price, thumbnail } = req.body;

  const product = generateNewProduct(title, price, thumbnail);
  res.send(product);
});


routes.put(baseProduct + "/actualizar/:id([0-9]+)", async (req, res) => {
  /**
   * Actualiza un producto
   */
  const productId = Number(req.params.id);

  const { title, price, thumbnail } = req.body;
  const product = updateProductById(productId, title, price, thumbnail);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
});

routes.delete(baseProduct + "/borrar/:id([0-9]+)", async (req, res) => {
  /**
   * Elimina un producto
   */
  const productId = Number(req.params.id);
  const product = deleteProductById(productId);

  if (product) {
    res.send({ product });
  } else {
    res.status(400).send({ error: "Producto no encontrado" });
  }
});

app.use(express.json())
app.use(express.static('public'))
app.use(baseUrl, routes);

app.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
