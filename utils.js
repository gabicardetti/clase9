export function generateRandomNumberFloat(min, max) {
    return Math.random() * (max - min + 1) + min;
};

export function generateRandomNumber(min, max) {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}